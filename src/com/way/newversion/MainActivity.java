package com.way.newversion;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import com.way.directionalviewpager.DirectionalViewPager;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity implements
		OnPageChangeListener {
	private DirectionalViewPager mDirectionalViewPager;
	private int mSize;
	private int mCurrentItem;
	private ImageView mBg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Set up the pager
		mDirectionalViewPager = (DirectionalViewPager) findViewById(R.id.pager);
		mBg = (ImageView) findViewById(R.id.mainBgImage);
		mDirectionalViewPager.setAdapter(new TestFragmentAdapter(
				getSupportFragmentManager()));
		mDirectionalViewPager.setOrientation(DirectionalViewPager.VERTICAL);// 设置方向垂直即可。
		mDirectionalViewPager.setOnPageChangeListener(this);
		
		int screenHeight = getScreenHeigh();
		Log.i("way", "Screen High = " + screenHeight);
		mSize = (int) (screenHeight * 0.05f);
		
		Log.i("way", "mSize = " + mSize);
		mCurrentItem = 0;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		//super.onSaveInstanceState(outState);
	}
	/**
	 * 获取状态栏高度
	 * 
	 * @return
	 */
	public int getStatusBarHeigh() {
		try {
			Class mainClass = Class.forName("com.android.internal.R$dimen");
			Object localObject = mainClass.newInstance();
			int statusBarHeightId = Integer.parseInt(mainClass
					.getField("status_bar_height").get(localObject).toString());
			int statusBarHeight = getResources().getDimensionPixelSize(
					statusBarHeightId);
			return statusBarHeight;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getResources().getDimensionPixelSize(R.dimen.status_bar_height);
	}

	public int getScreenHeigh() {
		DisplayMetrics dm = getResources().getDisplayMetrics();
		return dm.heightPixels;
	}

	@Override
	public void onPageScrollStateChanged(int state) {
		Log.i("way", "onPageScrollStateChanged...  state = " + state
				+ ",  mCurrentItem = " + mCurrentItem);
		if (state == ViewPager.SCROLL_STATE_IDLE) {
			mBg.setY(-mCurrentItem * mSize);
		}
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		Log.i("way", "onPageScrolled...  position=" + position
				+ ", positionOffset=" + positionOffset
				+ ", positionOffsetPixels=" + positionOffsetPixels);
		if (positionOffset == 0.0f)
			return;
		mBg.setY(-((position + positionOffset) * mSize));
	}

	@Override
	public void onPageSelected(int position) {
		Log.i("way", "onPageSelected....  position=" + position);
		mCurrentItem = position;
	}
}
