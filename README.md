#Android中可垂直方向滑动的ViewPager,使用Github中开源的DirectionalViewPager而做的一个小例子.背景也跟随滚动!
## 以下是测试截图:

![Screenshot 1](http://git.oschina.net/way/DirectionalViewPager/raw/master/1.png "Screenshot 1")

![Screenshot 2](http://git.oschina.net/way/DirectionalViewPager/raw/master/2.png "Screenshot 2")

![Screenshot 3](http://git.oschina.net/way/DirectionalViewPager/raw/master/3.png "Screenshot 3")

![Screenshot 4](http://git.oschina.net/way/DirectionalViewPager/raw/master/4.png "Screenshot 4")
